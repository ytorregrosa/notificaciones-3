package com.yth.mastermoviles.notificaciones3;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        texto = (TextView)findViewById(R.id.texto);

        Button color = (Button) findViewById(R.id.color);
        color.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                dialogoColor();
            }
        });

        Button tamanyo = (Button) findViewById(R.id.tamanyo);
        tamanyo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                dialogoTamanyo();
            }
        });
    }

    private void dialogoColor()
    {
        final String[] items = {"Blanco y Negro", "Negro y Blanco", "Negro y Verde"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selecciona el color");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Log.v("Debugger", ""+item);
                switch (item) {
                    case 0:
                        texto.setBackgroundColor(Color.rgb(255,255,255));
                        texto.setTextColor(Color.rgb(0,0,0));
                        break;
                    case 1:
                        texto.setBackgroundColor(Color.rgb(0,0,0));
                        texto.setTextColor(Color.rgb(255,255,255));
                        break;
                    case 2:
                        texto.setBackgroundColor(Color.rgb(0,0,0));
                        texto.setTextColor(Color.rgb(0,255,0));
                        break;

                }
            }
        });
        builder.show();
    }

    private void dialogoTamanyo()
    {
        final String[] items = {"Pequeño", "Normal", "Grande"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selecciona el tamaño");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        texto.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
                        break;
                    case 1:
                        texto.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                        break;
                    case 2:
                        texto.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        break;
                }
            }
        });
        builder.show();
    }
}
